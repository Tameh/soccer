package org.s2.exception;

import com.auth0.jwt.exceptions.SignatureVerificationException;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

public class ExceptionWrapper {
    public static String getError(Exception e) {

        if (e instanceof NoResultException) {
            return "Invalid Username or Password";
        } else if (e instanceof GeneralSecurityException) {
            return "Decrypt has problem" + "==>" + e.getMessage();
        } else if (e instanceof SignatureVerificationException) {
            return "The user is not logged in";
        } else if (e instanceof UnsupportedEncodingException) {
            return "Server has problem\nPlease Try Again\n" + e.getMessage();
        } else if (e instanceof IOException) {
            return "IOException: Connection Faild\n" + e.getMessage();
        } else if (e instanceof IllegalArgumentException) {
            return "Token doesn't find";
        } else {
            return e.getMessage();
        }
    }
}
