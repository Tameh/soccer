package org.s2.common;

import org.apache.commons.lang3.RandomStringUtils;

public class GenerateCode {

    /*use apache lang for take Generate String*/

    public static String getRandomString(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }
}
