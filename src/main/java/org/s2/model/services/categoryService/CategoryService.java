package org.s2.model.services.categoryService;

import org.s2.model.entity.gameCategory.GameCategory;
import org.s2.model.entity.gameCategory.categoryMode.CategoryMode;
import org.s2.model.repository.categoryRepository.CategoryRepo;
import org.s2.model.repository.categoryRepository.ModeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryRepo categoryRepo;
    @Autowired
    private ModeRepo modeRepo;

    public List<GameCategory> findCategotyList()throws Exception{
        return categoryRepo.selectCategories();
    }

    public List<GameCategory> findAll()throws Exception{
        return categoryRepo.selectAll();
    }

    public GameCategory findCategory(int id)throws Exception{
        return categoryRepo.selectOneCategory(id);
    }

    public void addCategory(GameCategory category)throws Exception{
        categoryRepo.save(category);
    }

    public void updateCategoryName(int categoryId,String name)throws Exception{
        GameCategory gameCategory = categoryRepo.selectOneCategory(categoryId);
        gameCategory.setName(name);
        categoryRepo.update(gameCategory);
    }

    public void updateCategoryState(int categoryId,String state)throws Exception{
        GameCategory gameCategory = categoryRepo.selectOneCategory(categoryId);
        gameCategory.setState(state);
        categoryRepo.update(gameCategory);
    }

    public void updateCategoryImage(int categoryId,String imgUrl)throws Exception{
        GameCategory gameCategory = categoryRepo.selectOneCategory(categoryId);
        gameCategory.setImgUrl(imgUrl);
        categoryRepo.update(gameCategory);
    }

    public void addCategoryMode(int categoryId,CategoryMode categoryMode)throws Exception{
        GameCategory gameCategory = categoryRepo.selectOneCategory(categoryId);
        gameCategory.getCategoryModes().add(categoryMode);
        categoryRepo.update(gameCategory);
    }

    public List<CategoryMode> findCategoryModes(int categoryId)throws Exception{
        return categoryRepo.selectOneWithChild(categoryId).getCategoryModes();
    }

    /*public void updateModeName(int categoryId,CategoryMode categoryMode)throws Exception{
        categoryRepo.update(modeId,name);
    }*/
    // monde

    /*public void updateModeImage(int modeId,String address){
        categoryRepo.updateImage(modeId,address);
    }*/
    //monde

}
