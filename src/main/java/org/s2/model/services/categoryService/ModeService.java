package org.s2.model.services.categoryService;

import org.s2.model.entity.gameCategory.categoryMode.CategoryMode;
import org.s2.model.repository.categoryRepository.ModeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ModeService {
    @Autowired
    private ModeRepo modeRepo;

    public CategoryMode findOne(int modeId)throws Exception{
        return modeRepo.selectOne(modeId);
    }
}
