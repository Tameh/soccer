package org.s2.model.services.sendEmails;

import org.s2.exception.ExceptionWrapper;
import org.s2.model.entity.profile.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmailSender {
    @Autowired
    private JavaMailSender javaMail;

    public void sendVerificationEmail( Profile profile) {
        Map map = new HashMap();

        String subject = "Please verify your registration";
        String senderName = "SoccerStars";
        String mailContent = "<p>Dear " + profile.getUserInfo().getUsername().toUpperCase() + "</p>";
        mailContent += "<p>Please click the link below to verify to your registration:</p>";

        String url = "localhost:8081/action/register/verifyCode?username=" + profile.getUserInfo().getUsername() + "&code=" + profile.getPrivateData().getGenerateCode();

        mailContent += "<h><a href=\"" + url + "\"" + ">VerifyProfile</a></h>";

        mailContent += "<p>Thank you<br>SoccerStars</p>";

        MimeMessage message = javaMail.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {

            helper.setFrom("socceremailtest@gmail.com", senderName);
            helper.setTo(profile.getPrivateData().getEmail());
            helper.setSubject(subject);
            helper.setText(mailContent, true);

        } catch (Exception e) {
            ExceptionWrapper.getError(e);
        }

        javaMail.send(message);

    }
}
