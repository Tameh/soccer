package org.s2.model.services.profileService;


import org.s2.common.AES;
import org.s2.common.GenerateCode;
import org.s2.common.MD5;
import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.mutableData.MutableData;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.publicData.PublicData;
import org.s2.model.repository.profileRepository.ProfileRepo;
import org.s2.model.services.sendEmails.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class ProfileServices {
    @Autowired
    private ProfileRepo profileRepo;
    @Autowired
    private EmailSender emailSender;

    public void saveNew(Profile profile) throws Exception {
        profile.getPrivateData().setGenerateCode(GenerateCode.getRandomString(20));
        /*Create GenerateCode for this user*/


        profile.getPrivateData().setInviteLink(AES.encrypt("Sesqszsc@123", profile.getUserInfo().getUsername()));

        profile.getUserInfo().setPassword(MD5.getMD5(profile.getUserInfo().getPassword()));
        profileRepo.save(profile);

        emailSender.sendVerificationEmail(profile);
        /*Send VerifyEmail*/
    }

    public void findGenerateCode(String username, String code) throws Exception {
        profileRepo.selectGenerateCode(username, code);
    }

    public void updateProfile(Profile profile) throws Exception {
        profileRepo.update(profile);
    }

    public void updateMutableData(int id, MutableData mutableData) throws Exception {
        Profile profile = profileRepo.selectById(id);
        profile.setMutableData(mutableData);
        profileRepo.update(profile);
    }

    public void updatePrivateDate(int id, PrivateData privateData) throws Exception {
        Profile profile = profileRepo.selectById(id);
        profile.setPrivateData(privateData);
        profileRepo.update(profile);
    }

    public void updatePublicData(int id, PublicData publicData) throws Exception {
        Profile profile = profileRepo.selectById(id);
        profile.setPublicData(publicData);
        profileRepo.update(profile);
    }

    public Profile findById(int id) throws Exception {
        return profileRepo.selectById(id);
    }

    public Profile findByUserId(String userId) throws Exception {
        return profileRepo.selectByUserId(userId);
    }

    public Profile findByEmail(String email) throws Exception {
        return profileRepo.selectByEmail(email);
    }

    public Profile findByUsername(String username) throws Exception {
        return profileRepo.selectByUsername(username);
    }

    public List<Profile> findAll() throws Exception {
        return profileRepo.selectAll();
    }

    public Object findInviteCode(String username) throws Exception {
        return AES.encrypt("Sesqszsc@123",profileRepo.selectByUsername(username).getUserInfo().getUsername());
    }

    public PublicData findPublicByUserId(String userId)throws Exception{
        return profileRepo.selectByUserId(userId).getPublicData();
    }

}
