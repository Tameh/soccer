package org.s2.model.services.loginService;

import org.s2.common.MD5;
import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.s2.model.repository.profileRepository.ProfileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LoginService {
    @Autowired
    private ProfileRepo profileRepo;

    public void saveUser(Profile profile) throws Exception {
        profileRepo.save(profile);
    }

    public void updatePassword(UserInfo userInfo) throws Exception {
        Profile  profile = profileRepo.selectByUsername(userInfo.getUsername());
        profile.getUserInfo().setPassword(MD5.getMD5(userInfo.getPassword()));
        profileRepo.update(profile);
    }

    public void findByUsername(UserInfo userInfo) throws Exception {
        profileRepo.selectByUsername(userInfo.getUsername());
    }

    public void findByUserInfo(UserInfo userInfo)throws Exception{
        userInfo.setPassword(MD5.getMD5(userInfo.getPassword()));

        profileRepo.selectByUserInfo(userInfo);
    }

}
