package org.s2.model.repository.categoryRepository;


import org.s2.model.entity.gameCategory.GameCategory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Repository
public class CategoryRepo {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(GameCategory category) throws Exception {
        entityManager.persist(category);
    }

    public void update(GameCategory gameCategory) throws Exception {
        entityManager.merge(gameCategory);
    }

    public List<GameCategory> selectAll() throws Exception {
        Query query = entityManager.createQuery("select o from gameCategory o join fetch o.categoryModes c");
        return query.getResultList();
    }

    public List<GameCategory> selectCategories() throws Exception {
        return entityManager.createQuery("select o from gameCategory o").getResultList();
    }

    public GameCategory selectOneCategory(int id) throws Exception {
        return entityManager.find(GameCategory.class, id);
    }

    public GameCategory selectOneWithChild(int categoryId)throws Exception{
        Query query = entityManager.createQuery("select o from gameCategory o join fetch o.categoryModes c where o.id=:n");
        query.setParameter("n",categoryId);
        return (GameCategory)query.getResultList().get(0);
    }

}
