package org.s2.model.repository.categoryRepository;

import org.s2.model.entity.gameCategory.categoryMode.CategoryMode;
import org.s2.model.entity.gameCategory.categoryMode.ModeConfig;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ModeRepo {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(CategoryMode categoryMode)throws Exception{
        entityManager.persist(categoryMode);
    }

    public void updateName(CategoryMode categoryMode)throws Exception{
        entityManager.merge(categoryMode);
    }

    public void updateImage(int modeId,String address)throws Exception{
        CategoryMode categoryMode = entityManager.find(CategoryMode.class , modeId);
        categoryMode.setImgUrl(address);
        entityManager.persist(categoryMode);
    }

    public void updateState(int modeId,String state)throws Exception{
        CategoryMode categoryMode = entityManager.find(CategoryMode.class,modeId);
        categoryMode.setState(state);
        entityManager.persist(categoryMode);
    }

    public void updateConfig(int modeId, ModeConfig modeConfig)throws Exception{
        CategoryMode categoryMode = entityManager.find(CategoryMode.class,modeId);
        categoryMode.setModeConfig(modeConfig);
        entityManager.persist(categoryMode);
    }

    public List<CategoryMode> selectAll()throws Exception{
        Query query = entityManager.createQuery("select o from categoryMode o");
        List<CategoryMode> categoryModeList = query.getResultList();
        return categoryModeList;
    }

    public CategoryMode selectOne(int id)throws Exception{
        return entityManager.find(CategoryMode.class,id);
    }

}
