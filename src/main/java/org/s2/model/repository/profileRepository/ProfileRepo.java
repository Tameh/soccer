package org.s2.model.repository.profileRepository;

import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.mutableData.MutableData;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.publicData.PublicData;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ProfileRepo {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(Profile profile) throws Exception {
        entityManager.persist(profile);
    }


    public void update(Profile profile) throws Exception {
        entityManager.merge(profile);
    }

    public Profile selectById(int id) throws Exception {
        Profile profile = entityManager.find(Profile.class, id);
        return profile;
    }

    public Profile selectByUserId(String userId) throws Exception {
        Query query = entityManager.createQuery("select o from profile o where o.userId=:n");
        query.setParameter("n", userId);
        Profile profile = (Profile) query.getResultList().get(0);
        return profile;
    }

    public Profile selectByEmail(String email) throws Exception {
        Query query = entityManager.createQuery("select o from profile o where o.privateData.email=:n");
        query.setParameter("n", email);
        return (Profile) query.getResultList().get(0);
    }

    public Profile selectByUsername(String username) throws Exception {
        Query query = entityManager.createQuery("select o from profile o where o.userInfo.username=:n");
        query.setParameter("n", username);
        return (Profile) query.getResultList().get(0);
    }

    public void selectByUserInfo(UserInfo userInfo) throws Exception {
        Query query = entityManager.createQuery("select o from profile o where o.userInfo.username=:u and o.userInfo.password=:p and o.userInfo.enable=1");
        query.setParameter("u", userInfo.getUsername()).setParameter("p", userInfo.getPassword()).getSingleResult();
    }


    public List<Profile> selectAll() throws Exception {
        Query query = entityManager.createNativeQuery("select * from profile_view");
        List<Profile> profileList = query.getResultList();
        return profileList;
    }

    public void updateMutable(int id, MutableData mutableData) throws Exception {
        Profile profile = entityManager.find(Profile.class, id);
        profile.setMutableData(mutableData);
        entityManager.persist(profile);
    }

    public void updatePrivate(int id, PrivateData privateData) throws Exception {
        Profile profile = entityManager.find(Profile.class, id);
        profile.setPrivateData(privateData);
        entityManager.persist(profile);
    }

    public void updatePublic(int id, PublicData publicData) throws Exception {
        Profile profile = entityManager.find(Profile.class, id);
        profile.setPublicData(publicData);
        entityManager.persist(profile);
    }

    public void selectGenerateCode(String username, String code) throws Exception {
        Query query = entityManager.createNativeQuery("select * from profile where username=:u and generatecode=:c");
        query.setParameter("u", username).setParameter("c", code);
        query = entityManager.createNativeQuery("insert into profile enable=1 where username=:uu");
        query.setParameter("uu", username);
    }
}
