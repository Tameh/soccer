package org.s2.model.entity.gameCategory.categoryMode;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "categoryMode")
@Entity(name = "categoryMode")
public class CategoryMode implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "number")
    private int id;
    @Column(columnDefinition = "number",name = "mode_order")
    private int order;
    @JsonIgnore
    @Version
    private int version;
    @Column(columnDefinition = "number")
    private int categoryId;
    @Column(columnDefinition = "number")
    private int botMinPlayer;
    @Column(columnDefinition = "number")
    private int minPlayer;
    @Column(columnDefinition = "number")
    private int maxPlayer;
    @Column(columnDefinition = "varchar2(20)",name = "mode_name")
    private String name;
    @Column(columnDefinition = "varchar2(20)",name = "mode_title")
    private String title;
    @Column(columnDefinition = "varchar2(20)",name = "mode_state")
    private String state;
    @Column(columnDefinition = "varchar2(40)")
    private String imageUrl;
    @Column(columnDefinition = "varchar2(10)")
    private String colorCode;
    @Embedded
    private ModeConfig modeConfig;

    public CategoryMode(){
        modeConfig = new ModeConfig();
    }

    public int getCategoryId() {
        return categoryId;
    }

    public CategoryMode setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public CategoryMode setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getColorCode() {
        return colorCode;
    }

    public CategoryMode setColorCode(String colorCode) {
        this.colorCode = colorCode;
        return this;
    }

    public int getId() {
        return id;
    }

    public CategoryMode setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CategoryMode setName(String name) {
        this.name = name;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CategoryMode setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getOrder() {
        return order;
    }

    public CategoryMode setOrder(int order) {
        this.order = order;
        return this;
    }

    public String getState() {
        return state;
    }

    public CategoryMode setState(String state) {
        this.state = state;
        return this;
    }

    public String getImgUrl() {
        return imageUrl;
    }

    public CategoryMode setImgUrl(String imgUrl) {
        this.imageUrl = imgUrl;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public CategoryMode setVersion(int version) {
        this.version = version;
        return this;
    }

    public ModeConfig getModeConfig() {
        return modeConfig;
    }

    public CategoryMode setModeConfig(ModeConfig modeConfig) {
        this.modeConfig = modeConfig;
        return this;
    }

    public int getBotMinPlayer() {
        return botMinPlayer;
    }

    public CategoryMode setBotMinPlayer(int botMinPlayer) {
        this.botMinPlayer = botMinPlayer;
        return this;
    }

    public int getMinPlayer() {
        return minPlayer;
    }

    public CategoryMode setMinPlayer(int minPlayer) {
        this.minPlayer = minPlayer;
        return this;
    }

    public int getMaxPlayer() {
        return maxPlayer;
    }

    public CategoryMode setMaxPlayer(int maxPlayer) {
        this.maxPlayer = maxPlayer;
        return this;
    }

    /*public int getCategoryId() {
        return categoryId;
    }

    public CategoryMode setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }*/
}

