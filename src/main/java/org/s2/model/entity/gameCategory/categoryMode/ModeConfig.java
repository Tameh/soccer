package org.s2.model.entity.gameCategory.categoryMode;



import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ModeConfig implements Serializable {
    private int prize;
    private int fansGain;
    private int matchTime;
    private int goalsNeeded;
    private int minLevel;
    private int entryFee;
    private String mapUrl;
    private String tournamentState;
    private String penaltyState;

    public int getMinLevel() {
        return minLevel;
    }

    public ModeConfig setMinLevel(int minLevel) {
        this.minLevel = minLevel;
        return this;
    }

    public int getEntryFee() {
        return entryFee;
    }

    public ModeConfig setEntryFee(int entryFee) {
        this.entryFee = entryFee;
        return this;
    }

    public int getPrize() {
        return prize;
    }

    public ModeConfig setPrize(int prize) {
        this.prize = prize;
        return this;
    }

    public int getFansGain() {
        return fansGain;
    }

    public ModeConfig setFansGain(int fansGain) {
        this.fansGain = fansGain;
        return this;
    }

    public int getMatchTime() {
        return matchTime;
    }

    public ModeConfig setMatchTime(int matchTime) {
        this.matchTime = matchTime;
        return this;
    }

    public int getGoalsNeeded() {
        return goalsNeeded;
    }

    public ModeConfig setGoalsNeeded(int goalsNeeded) {
        this.goalsNeeded = goalsNeeded;
        return this;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public ModeConfig setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
        return this;
    }

    public String getTournamentState() {
        return tournamentState;
    }

    public ModeConfig setTournamentState(String tournamentState) {
        this.tournamentState = tournamentState;
        return this;
    }

    public String getPenaltyState() {
        return penaltyState;
    }

    public ModeConfig setPenaltyState(String penaltyState) {
        this.penaltyState = penaltyState;
        return this;
    }
}
