package org.s2.model.entity.gameCategory;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.s2.model.entity.gameCategory.categoryMode.CategoryMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "gameCategory")
@Entity(name = "gameCategory")
public class  GameCategory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "number")
    private int id;
    @Column(columnDefinition = "varchar2(20)",name = "category_name")
    private String name;
    @Column(columnDefinition = "varchar2(20)",name = "category_title")
    private String title;
    @Column(columnDefinition = "varchar2(40)")
    private String imgUrl;
    @Column(columnDefinition = "varchar2(20)",name = "category_state")
    private String state;
    @Column(columnDefinition = "number",name = "category_order")
    private int order;
    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CategoryMode> categoryModes;
    @JsonIgnore
    @Version
    private int version;


    public int getId() {
        return id;
    }

    public GameCategory setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public GameCategory setName(String name) {
        this.name = name;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public GameCategory setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public GameCategory setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getState() {
        return state;
    }

    public GameCategory setState(String state) {
        this.state = state;
        return this;
    }

    public int getOrder() {
        return order;
    }

    public GameCategory setOrder(int order) {
        this.order = order;
        return this;
    }

    public List<CategoryMode> getCategoryModes() {
        return categoryModes;
    }

    public GameCategory setCategoryModes(List<CategoryMode> categoryModes) {
        this.categoryModes = categoryModes;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public GameCategory setVersion(int version) {
        this.version = version;
        return this;
    }
}

