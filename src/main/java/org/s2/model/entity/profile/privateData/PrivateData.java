package org.s2.model.entity.profile.privateData;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PrivateData implements Serializable {
    private String email;
    private String phoneNumber;
    private String gender;
    @JsonIgnore
    private String generateCode;
    private String inviteLink;

    public PrivateData() {
        email = "";
        phoneNumber = "";
        gender = "";
        generateCode = "";
        inviteLink = "";
    }

    public String getEmail() {
        return email;
    }

    public PrivateData setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public PrivateData setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public PrivateData setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getGenerateCode() {
        return generateCode;
    }

    public PrivateData setGenerateCode(String generateCode) {
        this.generateCode = generateCode;
        return this;
    }

    public String getInviteLink() {
        return inviteLink;
    }

    public PrivateData setInviteLink(String inviteLink) {
        this.inviteLink = inviteLink;
        return this;
    }
}
