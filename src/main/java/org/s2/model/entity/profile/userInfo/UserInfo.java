package org.s2.model.entity.profile.userInfo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.s2.model.entity.profile.privateData.PrivateData;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserInfo implements Serializable {
    @Column(columnDefinition = "varchar2(20)",name = "username")
    private String username;
    @Column(columnDefinition = "varchar2(32)")
    private String password;
    @JsonIgnore
    @Column(columnDefinition = "number")
    private short enable = 0;

    public String getUsername() {
        return username;
    }

    public UserInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserInfo setPassword(String password) {
        this.password = password;
        return this;
    }
    public short getEnable() {
        return enable;
    }

    public UserInfo setEnable(short enable) {
        this.enable = enable;
        return this;
    }
}
