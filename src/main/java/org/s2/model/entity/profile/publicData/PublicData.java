package org.s2.model.entity.profile.publicData;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

@Embeddable
public class PublicData implements Serializable {
    private String imgUrl;
    private String countryFlagUrl;
    private String country;
    private int lvl;
    private int fans;
    private int currentGold;
    private int currentSilver;
    @Embedded
    private PlayerStats stats;

    public PublicData(){
        stats = new PlayerStats();
        imgUrl="";
        countryFlagUrl="";
        country="";
    }

    public PlayerStats getStats() {
        return stats;
    }

    public PublicData setStats(PlayerStats stats) {
        this.stats = stats;
        return this;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public PublicData setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getCountryFlagUrl() {
        return countryFlagUrl;
    }

    public PublicData setCountryFlagUrl(String countryFlagUrl) {
        this.countryFlagUrl = countryFlagUrl;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public PublicData setCountry(String country) {
        this.country = country;
        return this;
    }

    public int getLvl() {
        return lvl;
    }

    public PublicData setLvl(int lvl) {
        this.lvl = lvl;
        return this;
    }

    public int getFans() {
        return fans;
    }

    public PublicData setFans(int fans) {
        this.fans = fans;
        return this;
    }

    public int getCurrentGold() {
        return currentGold;
    }

    public PublicData setCurrentGold(int currentGold) {
        this.currentGold = currentGold;
        return this;
    }

    public int getCurrentSilver() {
        return currentSilver;
    }

    public PublicData setCurrentSilver(int currentSilver) {
        this.currentSilver = currentSilver;
        return this;
    }
}
