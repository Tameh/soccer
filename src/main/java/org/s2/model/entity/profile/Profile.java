package org.s2.model.entity.profile;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.s2.model.entity.profile.mutableData.MutableData;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.publicData.PublicData;
import org.s2.model.entity.profile.userInfo.UserInfo;

import javax.persistence.*;
import java.io.Serializable;


@Table(name = "profile",uniqueConstraints =@UniqueConstraint(columnNames ={"username","userId"}))
@Entity(name = "profile")
public class Profile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "number")
    private int id;
    @Column(columnDefinition = "varchar2(20)",name = "userId")
    private String userId;
    @Embedded
    private PublicData publicData;
    @Embedded
    private MutableData mutableData;
    @Embedded
    @JsonIgnore
    private PrivateData privateData;
    @Embedded
    @JsonIgnore
    private UserInfo userInfo;
    @JsonIgnore
    @Version
    private int version;

    public Profile(){
        publicData = new PublicData();
        privateData = new PrivateData();
        mutableData = new MutableData();
        userInfo = new UserInfo();
        userId="";
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public Profile setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

    public int getId() {
        return id;
    }

    public Profile setId(int id) {
        this.id = id;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public Profile setVersion(int version) {
        this.version = version;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public Profile setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public PublicData getPublicData() {
        return publicData;
    }

    public Profile setPublicData(PublicData publicData) {
        this.publicData = publicData;
        return this;
    }

    public PrivateData getPrivateData() {
        return privateData;
    }

    public Profile setPrivateData(PrivateData privateData) {
        this.privateData = privateData;
        return this;
    }

    public MutableData getMutableData() {
        return mutableData;
    }

    public Profile setMutableData(MutableData mutableData) {
        this.mutableData = mutableData;
        return this;
    }
}

