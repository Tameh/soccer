package org.s2.model.entity.profile.mutableData;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

@Embeddable
public class MutableData implements Serializable {
    @Embedded
    private SoundSetting soundSetting;
    @Embedded
    private HelpSetting helpSetting;
    private String autoRotate;

    public MutableData(){
        soundSetting = new SoundSetting();
        helpSetting = new HelpSetting();
        autoRotate="";
    }

    public SoundSetting getSoundSetting() {
        return soundSetting;
    }

    public MutableData setSoundSetting(SoundSetting soundSetting) {
        this.soundSetting = soundSetting;
        return this;
    }

    public HelpSetting getHelpSetting() {
        return helpSetting;
    }

    public MutableData setHelpSetting(HelpSetting helpSetting) {
        this.helpSetting = helpSetting;
        return this;
    }

    public String isAutoRotate() {
        return autoRotate;
    }

    public MutableData setAutoRotate(String autoRotate) {
        this.autoRotate = autoRotate;
        return this;
    }
}
