package org.s2.model.entity.profile.mutableData;


import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SoundSetting implements Serializable {
    private String soundEffects;
    private String announcerSound;
    private String music;
    private String vibration;

    public SoundSetting(){
        soundEffects="";
        announcerSound="";
        music="";
        vibration="";
    }

    public String isSoundEffects() {
        return soundEffects;
    }

    public SoundSetting setSoundEffects(String soundEffects) {
        this.soundEffects = soundEffects;
        return this;
    }

    public String isAnnouncerSound() {
        return announcerSound;
    }

    public SoundSetting setAnnouncerSound(String announcerSound) {
        this.announcerSound = announcerSound;
        return this;
    }

    public String isMusic() {
        return music;
    }

    public SoundSetting setMusic(String music) {
        this.music = music;
        return this;
    }

    public String isVibration() {
        return vibration;
    }

    public SoundSetting setVibration(String vibration) {
        this.vibration = vibration;
        return this;
    }
}
