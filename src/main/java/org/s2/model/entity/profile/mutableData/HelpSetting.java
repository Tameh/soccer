package org.s2.model.entity.profile.mutableData;


import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class HelpSetting implements Serializable {
    private String seenHelp;
    private String seenGameTutorial;
    private String seenInfoTutorial;

    public String isSeenHelp() {
        return seenHelp;
    }

    public HelpSetting setSeenHelp(String seenHelp) {
        this.seenHelp = seenHelp;
        return this;
    }

    public String isSeenGameTutorial() {
        return seenGameTutorial;
    }

    public HelpSetting setSeenGameTutorial(String seenGameTutorial) {
        this.seenGameTutorial = seenGameTutorial;
        return this;
    }

    public String isSeenInfoTutorial() {
        return seenInfoTutorial;
    }

    public HelpSetting setSeenInfoTutorial(String seenInfoTutorial) {
        this.seenInfoTutorial = seenInfoTutorial;
        return this;
    }
}
