package org.s2.model.entity.profile.publicData;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PlayerStats implements Serializable {
    private int totalWinning;
    private int games;
    private int gamesWon;
    private int percentage;
    private int tournamentWon;
    private int winStreak;
    private int goalsScored;
    private int championWon;
    private int bestPenaltyScore;


    public int getTotalWinning() {
        return totalWinning;
    }

    public PlayerStats setTotalWinning(int totalWinning) {
        this.totalWinning = totalWinning;
        return this;
    }

    public int getGames() {
        return games;
    }

    public PlayerStats setGames(int games) {
        this.games = games;
        return this;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public PlayerStats setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
        return this;
    }

    public int getPercentage() {
        return percentage;
    }

    public PlayerStats setPercentage(int percentage) {
        this.percentage = percentage;
        return this;
    }

    public int getTournamentWon() {
        return tournamentWon;
    }

    public PlayerStats setTournamentWon(int tournamentWon) {
        this.tournamentWon = tournamentWon;
        return this;
    }

    public int getWinStreak() {
        return winStreak;
    }

    public PlayerStats setWinStreak(int winStreak) {
        this.winStreak = winStreak;
        return this;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public PlayerStats setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
        return this;
    }

    public int getChampionWon() {
        return championWon;
    }

    public PlayerStats setChampionWon(int championWon) {
        this.championWon = championWon;
        return this;
    }

    public int getBestPenaltyScore() {
        return bestPenaltyScore;
    }

    public PlayerStats setBestPenaltyScore(int bestPenaltyScore) {
        this.bestPenaltyScore = bestPenaltyScore;
        return this;
    }
}
