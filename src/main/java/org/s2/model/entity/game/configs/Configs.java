package org.s2.model.entity.game.configs;

import org.s2.model.entity.game.configs.multiLanguage.MultiLanguage;

public class Configs {
    private int serverTimeOut;
    private int amountOfCoinToGold;
    private int adsCoinReward;
    private int inviteCoinReward;
    private String suppDefualtMsg;
    private HelpConfig helpConfig;
    private MultiLanguage multiLanguage;
    private Levels levels;
    private LevelUpReward levelUpReward;
    private XpState xpState;

    public int getServerTimeOut() {
        return serverTimeOut;
    }

    public Configs setServerTimeOut(int serverTimeOut) {
        this.serverTimeOut = serverTimeOut;
        return this;
    }

    public int getAmountOfCoinToGold() {
        return amountOfCoinToGold;
    }

    public Configs setAmountOfCoinToGold(int amountOfCoinToGold) {
        this.amountOfCoinToGold = amountOfCoinToGold;
        return this;
    }

    public int getAdsCoinReward() {
        return adsCoinReward;
    }

    public Configs setAdsCoinReward(int adsCoinReward) {
        this.adsCoinReward = adsCoinReward;
        return this;
    }

    public int getInviteCoinReward() {
        return inviteCoinReward;
    }

    public Configs setInviteCoinReward(int inviteCoinReward) {
        this.inviteCoinReward = inviteCoinReward;
        return this;
    }

    public String getSuppDefualtMsg() {
        return suppDefualtMsg;
    }

    public Configs setSuppDefualtMsg(String suppDefualtMsg) {
        this.suppDefualtMsg = suppDefualtMsg;
        return this;
    }

    public HelpConfig getHelpConfig() {
        return helpConfig;
    }

    public Configs setHelpConfig(HelpConfig helpConfig) {
        this.helpConfig = helpConfig;
        return this;
    }

    public MultiLanguage getMultiLanguage() {
        return multiLanguage;
    }

    public Configs setMultiLanguage(MultiLanguage multiLanguage) {
        this.multiLanguage = multiLanguage;
        return this;
    }

    public Levels getLevels() {
        return levels;
    }

    public Configs setLevels(Levels levels) {
        this.levels = levels;
        return this;
    }

    public LevelUpReward getLevelUpReward() {
        return levelUpReward;
    }

    public Configs setLevelUpReward(LevelUpReward levelUpReward) {
        this.levelUpReward = levelUpReward;
        return this;
    }

    public XpState getXpState() {
        return xpState;
    }

    public Configs setXpState(XpState xpState) {
        this.xpState = xpState;
        return this;
    }
}
