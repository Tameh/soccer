package org.s2.model.entity.game.configs;

import java.util.List;

public class Levels {
    private static List<Integer> levels;

    static {
        biuldLevels();
    }

    public List<Integer> getLevels() {
        return levels;
    }

    public Levels setLevels(List<Integer> levels) {
        this.levels = levels;
        return this;
    }

    public static void biuldLevels(){
        int base = 270;
        for (int i =0;i<=50;i++){
            levels.add(base);
            base = (int) (base + base * 1.2);
        }
    }
}
