package org.s2.model.entity.game;

import org.s2.model.entity.game.configs.Configs;
import org.s2.model.entity.game.serverConfigs.ServerConfigs;

public class Game {
    private int id;
    private String name;
    private Configs configs;
    private ServerConfigs serverConfigs;
    private int version;

    public int getId() {
        return id;
    }

    public Game setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Game setName(String name) {
        this.name = name;
        return this;
    }

    public Configs getConfigs() {
        return configs;
    }

    public Game setConfigs(Configs configs) {
        this.configs = configs;
        return this;
    }

    public ServerConfigs getServerConfigs() {
        return serverConfigs;
    }

    public Game setServerConfigs(ServerConfigs serverConfigs) {
        this.serverConfigs = serverConfigs;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public Game setVersion(int version) {
        this.version = version;
        return this;
    }
}
