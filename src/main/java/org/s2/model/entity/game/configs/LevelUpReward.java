package org.s2.model.entity.game.configs;

import java.util.List;

public class LevelUpReward {
    private String rewardType;
    private List<Integer> rewards;

    public String getRewardType() {
        return rewardType;
    }

    public LevelUpReward setRewardType(String rewardType) {
        this.rewardType = rewardType;
        return this;
    }

    public List<Integer> getRewards() {
        return rewards;
    }

    public LevelUpReward setRewards(List<Integer> rewards) {
        this.rewards = rewards;
        return this;
    }
}
