package org.s2.model.entity.game.configs;

import java.util.List;

public class HelpConfig {
    private List<String> imgUrl;
    private String titleUrl;

    public List<String> getImgUrl() {
        return imgUrl;
    }

    public HelpConfig setImgUrl(List<String> imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getTitleUrl() {
        return titleUrl;
    }

    public HelpConfig setTitleUrl(String titleUrl) {
        this.titleUrl = titleUrl;
        return this;
    }
}
