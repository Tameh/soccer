package org.s2.model.entity.game.configs;

public class XpState {
    private int xpPerGoal;
    private int xpPerWin;
    private int xpPerLose;
    private int tournamentWinXp;

    public int getXpPerGoal() {
        return xpPerGoal;
    }

    public XpState setXpPerGoal(int xpPerGoal) {
        this.xpPerGoal = xpPerGoal;
        return this;
    }

    public int getXpPerWin() {
        return xpPerWin;
    }

    public XpState setXpPerWin(int xpPerWin) {
        this.xpPerWin = xpPerWin;
        return this;
    }

    public int getXpPerLose() {
        return xpPerLose;
    }

    public XpState setXpPerLose(int xpPerLose) {
        this.xpPerLose = xpPerLose;
        return this;
    }

    public int getTournamentWinXp() {
        return tournamentWinXp;
    }

    public XpState setTournamentWinXp(int tournamentWinXp) {
        this.tournamentWinXp = tournamentWinXp;
        return this;
    }
}
