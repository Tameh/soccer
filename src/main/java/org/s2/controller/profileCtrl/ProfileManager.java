package org.s2.controller.profileCtrl;

import org.s2.controller.loginCtrl.VerifyUser;
import org.s2.exception.ExceptionWrapper;
import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.mutableData.MutableData;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.publicData.PublicData;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.s2.model.services.profileService.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/profileManager")
public class ProfileManager {
    @Autowired
    private ProfileServices profileServices;
    @Autowired
    private VerifyUser verifyUser;

    // TODO: 1/3/2021 Mehran in method nabayad pak  beshe?

    @RequestMapping("/addProfile")
    public Object addProfile(@ModelAttribute Profile profile, @ModelAttribute PublicData publicData, @ModelAttribute PrivateData privateData) {
        profile.setPublicData(publicData).setPrivateData(privateData);

        Map<String, Object> map = new HashMap<>();

        try {
            profileServices.saveNew(profile);
            map.put("result", profileServices.findById(profile.getId()));
            map.put("exception", "");
        } catch (Exception e) {
            map.put("result", "");
            map.put("exception", e.getMessage());
        }
        return map;
    }


    @RequestMapping("/getProfile")
    public Object getProfile(@ModelAttribute UserInfo userInfo, @CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {

            verifyUser.addToken(token);
            /*Check User for Enter this method*/

            map.put("result", profileServices.findByUserId(userInfo.getUsername()));
            map.put("exception", "");
        } catch (Exception e) {
            map.put("result", "");
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/getProfileById")
    public Object getProfileById(int id, @CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {

            verifyUser.addToken(token);

            map.put("result", profileServices.findById(id));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editProfile")
    public Object editProfile(@ModelAttribute Profile profile, @ModelAttribute PublicData publicData, @ModelAttribute PrivateData privateData, @ModelAttribute MutableData mutableData,@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        profile.setPrivateData(privateData).setPublicData(publicData).setMutableData(mutableData);

        try {
            verifyUser.addToken(token);

            profileServices.updateProfile(profile);
            map.put("result", profileServices.findById(profile.getId()));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editSetting")
    public Object editSetting(int id, @ModelAttribute MutableData mutableData,@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();
        try {
            verifyUser.addToken(token);

            profileServices.updateMutableData(id, mutableData);
            map.put("result", "done");
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editPlayerStats")
    public Object editPlayerStats(int id, @ModelAttribute PublicData publicData,@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {
            verifyUser.addToken(token);

            profileServices.updatePublicData(id, publicData);
            map.put("result", profileServices.findById(id));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editPersonalInfo")
    public Object editPersonalInfo(int id, @ModelAttribute PrivateData privateData,@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {
            verifyUser.addToken(token);

            profileServices.updatePrivateDate(id, privateData);
            map.put("result", profileServices.findById(id));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/getAllUsers")
    public Object findAllUsers(@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {
            verifyUser.addToken(token);

            map.put("result", profileServices.findAll());
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/getInviteCode")
    public Object getInviteCode(String username,@CookieValue(value = "requirment", defaultValue = "false") String token) {
        Map<String, Object> map = new HashMap<>();

        try {
            verifyUser.addToken(token);

            map.put("result", profileServices.findInviteCode(username));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));
        }
        return map;
    }

    /*@RequestMapping("/findUserByUserId")
    public Object findUserByUserId(@CookieValue(value = "requirment", defaultValue = "not find cookie") String token){
        Map<String, Object> map = new HashMap<>();

        try {


            String userId = check.getUser(token);
            map.put("result",profileServices.findByUserId(userId));
            map.put("exception",null);
        }catch (Exception e){
            map.put("result",null);
            map.put("exception",e.getMessage());
        }
        return map;
    }*/

}
