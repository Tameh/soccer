package org.s2.controller.categoryCtrl;


import org.s2.model.entity.gameCategory.GameCategory;
import org.s2.model.entity.gameCategory.categoryMode.CategoryMode;
import org.s2.model.entity.gameCategory.categoryMode.ModeConfig;
import org.s2.model.services.categoryService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/categoryManager")
public class CategoryManager {
    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/test")
    public String aa() {
        return "Helooo";
    }  // garbage


    @RequestMapping("/getCategories")
    public Object getCategories() {
        Map map = new HashMap();
        try {
            map.put("result",categoryService.findCategotyList());
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception",e.getMessage());
        }
        return map;
    }

    @RequestMapping("/getCategory")
    public Object getCategory(int id) {
        Map map = new HashMap();
        try {
            map.put("result",categoryService.findCategory(id));
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result",null);
            map.put("exception",e.getMessage());
        }
        return map;
    }

    @RequestMapping("/createCategory")
    public Object createCategory(@ModelAttribute GameCategory gameCategory) {
        Map map = new HashMap();
        try {
            categoryService.addCategory(gameCategory);
            map.put("result", "done");
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }


    @RequestMapping("/createNewMode")
    public Object CreateNewMode(@ModelAttribute CategoryMode categoryMode, @ModelAttribute ModeConfig modeConfig) {
        categoryMode.setModeConfig(modeConfig);
        Map map = new HashMap();
        try {
            categoryService.addCategoryMode(categoryMode.getCategoryId(),categoryMode);
            map.put("result", "done");
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/getModes")
    public Object getCategoryModes(int categoryId) {
        Map map = new HashMap();
        try {
            map.put("result", categoryService.findCategoryModes(categoryId));
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editCategoryName")
    public Object editCategoryName(int categoryId,String name) {
        Map map = new HashMap();
        try {
            categoryService.updateCategoryName(categoryId,name);
            map.put("result", "done");
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result",null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editCategoryState")
    public Object editCategoryState(int categoryId,String state) {
        Map map = new HashMap();
        try {
            categoryService.updateCategoryState(categoryId,state);
            map.put("result", "done");
            map.put("exception",null);
        } catch (Exception e) {
            map.put("result",null);
            map.put("exception", e.getMessage());
        }
        return map;
    }

    @RequestMapping("/editCategoryImage")
    public Object editCategoryImage(int categoryId,String imgUrl) {
        Map map = new HashMap();
        try {
            categoryService.updateCategoryImage(categoryId,imgUrl);
            map.put("result", "done");
            map.put("exception", null);
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", e.getMessage());
        }
        return map;
    }


    /*@RequestMapping("/editModeName")
    public void editModeName(int categoryId, @ModelAttribute CategoryMode categoryMode) {
        categoryService.updateModeName(categoryId, categoryMode);
    }*/

    /*@RequestMapping("/editModeImage")
    public void editModeImage(int modeId, String address) {
        categoryService.updateModeImage(modeId, address);
    }*/


    /********************************************************************************************************/
    public CategoryMode createCategoryMode(String name, String address) {
        return new CategoryMode().setName(name).setImgUrl(address);
    }
}