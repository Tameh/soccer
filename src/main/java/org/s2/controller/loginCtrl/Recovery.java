package org.s2.controller.loginCtrl;

import org.s2.exception.ExceptionWrapper;
import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.s2.model.services.loginService.LoginService;
import org.s2.model.services.profileService.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/recovery")
public class Recovery {
    @Autowired
    private ProfileServices profileServices;
    @Autowired
    private LoginService loginService;

    @RequestMapping("/forgetPassword")
    public Object ForgetPassword(@ModelAttribute PrivateData privateData, @ModelAttribute UserInfo userInfo) {
        Map<String, Object> map = new HashMap<>();

        try {
            Profile profile = profileServices.findByEmail(privateData.getEmail());
            /*Frist get profile(email)*/

            userInfo.setUsername(profile.getUserInfo().getUsername());
            /*Second add username */

            loginService.updatePassword(userInfo);
            /*Third get password from client*/

            map.put("result", profileServices.findByUsername(profile.getUserInfo().getUsername()));
            map.put("exception", null);
            /*Finaly return his profile*/
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", "Recovery Failed!" + ExceptionWrapper.getError(e));
        }
        return map;
    }
}
