package org.s2.controller.loginCtrl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.s2.common.AES;
import org.s2.exception.ExceptionWrapper;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.s2.model.services.loginService.LoginService;
import org.s2.model.services.profileService.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class Login {
    @Autowired
    private ProfileServices profileServices;
    @Autowired
    private LoginService loginService;

    private Cookie cookie = null;

    @RequestMapping("/service")
    public Object service(@ModelAttribute UserInfo userInfo, HttpServletRequest req, HttpServletResponse resp) {
        Map<String, Object> map = new HashMap<>();

        try {
            loginService.findByUserInfo(userInfo);
            /*User logged in*/
            String tk = JWT.create()
                    .withIssuer("MMAGroup")
                    .withClaim("userId", userInfo.getUsername())
                    .withClaim("Autorize", "Sesqszsc@autorize")
                    .sign(Algorithm.HMAC256("Sesqszsc@123"));
            /*Token create with JWT structure*/

            String token = AES.encrypt("Sesqszsc@123", tk);
            /*Encrypted with AES*/


            cookie = new Cookie("requirment", token);
            cookie.setPath("/action/");
            resp.addCookie(cookie);
            /*Add on user cookie */

            map.put("result", profileServices.findByUsername(userInfo.getUsername()));
            map.put("exception", null);
            /*Return client profile*/
        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));
            /*Return exception message*/
        }
        return map;
    }
}
