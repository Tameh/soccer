package org.s2.controller.loginCtrl;

import org.s2.exception.ExceptionWrapper;
import org.s2.model.entity.profile.Profile;
import org.s2.model.entity.profile.privateData.PrivateData;
import org.s2.model.entity.profile.userInfo.UserInfo;
import org.s2.model.services.profileService.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/register")
public class Register {
    @Autowired
    private ProfileServices profileServices;

    @RequestMapping("/addUser")
    public Object addUser(@ModelAttribute Profile profile, @ModelAttribute UserInfo userInfo, @ModelAttribute PrivateData privateData) {

        Map<String, Object> map = new HashMap<>();

        profile.setPrivateData(privateData).setUserInfo(userInfo);

        try {

            // TODO: 12/6/2020 Mehran Email is unique
            profileServices.saveNew(profile);
            /*Profile creation done*/

            map.put("result", profileServices.findByUsername(userInfo.getUsername()));
            map.put("exception", null);

        } catch (Exception e) {
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));
        }
        return map;
    }

    @RequestMapping("/verifyCode")
    public String verifyCode(String username, String code) {

        String result = "";
        try {

            profileServices.findGenerateCode(username, code);
            result = "Verify Profile Successed";

        } catch (Exception e) {
            result = "Verify Profile Failed\n" + ExceptionWrapper.getError(e);
        }
        return result;
    }
}
