package org.s2.controller.loginCtrl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.s2.common.AES;
import org.s2.exception.ExceptionWrapper;
import org.s2.model.services.profileService.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/verifyUser")
public class VerifyUser {
    @Autowired
    private ProfileServices profileServices;

    @RequestMapping("/check")
    public Object check(@CookieValue(value = "requirment", defaultValue = "false") String token, HttpServletResponse resp) {
        Map<String, Object> map = new HashMap<>();
        /*Get token from client..*/

        try {
            String tk = AES.decrypt("Sesqszsc@123", token);
            /*Decrypt this token with AES decrypt*/

            JWTVerifier verifier = JWT.require(Algorithm.HMAC256("Sesqszsc@123"))
                    .withIssuer("MMAGroup")
                    .build();//Check Kardane Token

            DecodedJWT jwt = verifier.verify(tk);
            if (jwt.getClaim("Autorize").asString().equals("Sesqszsc@autorize")) {
                try {
                    /*if this block is true user can break*/
                    map.put("result", profileServices.findByUsername(jwt.getClaim("userId").asString()));
                    map.put("exception", null);

                } catch (Exception e) {
                    /*user want create fake token*/
                    System.out.println("Authorize faild...");
                    System.out.println("Go back login again");
                    map.put("result", null);
                    map.put("exception", ExceptionWrapper.getError(e));


                }
            } else {
                /*Server did not create token for this user*/
                System.out.println("Go back login again");
                resp.sendRedirect("/action/login/service");
            }


        } catch (IllegalArgumentException f) {
            /*Token doesn't find*/
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(f));
        } catch (SignatureVerificationException x) {
            /*The user is not logged in*/
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(x));

        } catch (UnsupportedEncodingException e) {
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));

        } catch (IOException e) {
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));

        } catch (GeneralSecurityException e) {
            /*Return decrypt exception message*/
            map.put("result", null);
            map.put("exception", ExceptionWrapper.getError(e));
        }
        return map;
    }

    public void addToken(String token) throws Exception {

        /*This method return a user of players*/
        JWTVerifier veriﬁer = JWT.require(Algorithm.HMAC256("Sesqszsc@123"))
                .withIssuer("MMAGroup")
                .build();//Check Kardane Token

        DecodedJWT jwt = veriﬁer.verify(token);

        if (!(jwt.getClaim("Autorize").asString().equals("Sesqszsc@autorize"))) {
            throw new Exception();
        }
    }
}
